var io = require('socket.io');
var http = require('http');
var app = http .createServer();
var io = io.listen(app);
app.listen(8008);
var users = [];

// включаем сервер
io.sockets.on('connection', function (socket) {

	// приветствуем подключившегося 
	socket.emit('eventClient', { data: "connect"});
	// добавляем уникальный id как имя
	var usernum = socket.id;
	if(users.indexOf(usernum) == '-1'){
		users.push(usernum);
	}

	socket.on('eventServer', function (data) {
		if(data.data == 'ping'){
			console.log(data.data);
			socket.emit('eventClient', { data: 'pong' });
		}else{
			// console.log(usernum+" - "+data.coordX+":"+data.coordY);
			data.name = usernum;
			// передаем данные всем пользователям
			io.sockets.emit('eventClient', { data: data});	
		}
	});
	
	// отключаемся
	socket.on('disconnect', function (data) {
		console.log('delete '+users[users.indexOf(socket.id)]);
		// отправляем id с прифексом для удаления
		io.sockets.emit('eventClient', { del: "del_"+users[users.indexOf(socket.id)]});
		// удаляем пользователя
		users.splice(users.indexOf(socket.id), 1);
		console.log('user disconnected');
	});
});

console.log("start server");
